package com.pragmaticingenieria.parqueadero.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.pragmaticingenieria.parqueadero.R;

public class CarroCursorAdapter extends CursorAdapter {

    @SuppressWarnings("deprecation")
    public CarroCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        return inflater.inflate(R.layout.elemento_carro, viewGroup,false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView placa = (TextView) view.findViewById(R.id.cell_carro);
        placa.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));
        TextView color = (TextView) view.findViewById(R.id.cell_color);
        color.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));
        Button bt = (Button) view.findViewById(R.id.btn_editar_carro);
        bt.setTag(placa.getText().toString());
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, view.getTag().toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

}