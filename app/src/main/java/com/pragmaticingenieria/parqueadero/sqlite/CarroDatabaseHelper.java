package com.pragmaticingenieria.parqueadero.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CarroDatabaseHelper {

    private static final String TAG = CarroDatabaseHelper.class.getSimpleName();

    // CONFIGURACION DE LA BASE DE DATOS
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_placa = "carro_database.db";

    // CONFIGURACION DE LA TABLA
    private static final String TABLE_NAME = "carros";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_PLACA = "placa";
    private static final String COLUMN_COLOR = "color";

    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase database;

    // esto es una clase contenedora
    // DatabaseOpenHelper clase realizar operaciones CRUD de base de datos
    public CarroDatabaseHelper(Context aContext) {
        openHelper = new DatabaseOpenHelper(aContext);
        database = openHelper.getWritableDatabase();
    }

    public void insertData (String placa, String color) {

        // ContentValues evita errores en sql

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PLACA, placa);
        contentValues.put(COLUMN_COLOR, color);
        database.insert(TABLE_NAME, null, contentValues);

    }

    public int deleteRow(String id) {
        return database.delete(TABLE_NAME, COLUMN_ID + "=" + id, null);
    }

    public int updateRow(String id, String placa, String color
                         ) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_PLACA, placa);
        cv.put(COLUMN_COLOR, color);

        return database.update(TABLE_NAME, cv, COLUMN_ID + "=" + id, null);
    }


    public Cursor getAllData () {

        String buildSQL = "SELECT * FROM " + TABLE_NAME;

        Log.d(TAG, "Listado Carros SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper(Context aContext) {
            super(aContext, DATABASE_placa, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            // se crea la tabla

            String buildSQL = "CREATE TABLE " + TABLE_NAME + "( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_PLACA + " TEXT NOT NULL, " + COLUMN_COLOR +  " TEXT NOT NULL);";

            Log.d(TAG, "Nuevo Carro SQL: " + buildSQL);

            sqLiteDatabase.execSQL(buildSQL);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            //aqui se actualiza la tabla

            String buildSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

            Log.d(TAG, "Eliminar Tabla SQL: " + buildSQL);

            sqLiteDatabase.execSQL(buildSQL);

            onCreate(sqLiteDatabase);
        }
    }


}
