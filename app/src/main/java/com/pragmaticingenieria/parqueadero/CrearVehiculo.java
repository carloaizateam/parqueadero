package com.pragmaticingenieria.parqueadero;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pragmaticingenieria.parqueadero.sqlite.CarroDatabaseHelper;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CrearVehiculo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CrearVehiculo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CrearVehiculo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CarroDatabaseHelper databaseHelper;

    private Button btnCrearVeh;

    private OnFragmentInteractionListener mListener;

    public CrearVehiculo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CrearVehiculo.
     */
    // TODO: Rename and change types and number of parameters
    public static CrearVehiculo newInstance(String param1, String param2) {
        CrearVehiculo fragment = new CrearVehiculo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        databaseHelper = new CarroDatabaseHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crear_vehiculo, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnCrearVeh= (Button) getActivity().findViewById(R.id.btn_guardar);

        btnCrearVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crearVehiculo();
            }
        });
    }

    public void crearVehiculo()
    {
        TextView txtPlaca= (TextView) getActivity().findViewById(R.id.txt_placa);
        TextView txtColor= (TextView) getActivity().findViewById(R.id.txt_color);


        if (txtPlaca.length() != 0 && txtColor.length() != 0)
        {
            databaseHelper.insertData(txtPlaca.getText().toString(), txtColor.getText().toString());

            Toast.makeText(getContext(), "Se ha guardado el carro", Toast.LENGTH_LONG).show();

        }
        else
        {
            Toast.makeText(getContext(), "Debe ingresar los datos obligatorios", Toast.LENGTH_LONG).show();
        }
    }
}
