package com.pragmaticingenieria.parqueadero;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.pragmaticingenieria.entidades.Carro;
import com.pragmaticingenieria.parqueadero.sqlite.CarroCursorAdapter;
import com.pragmaticingenieria.parqueadero.sqlite.CarroDatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

public class ListarVehiculos extends ListFragment {

    private CarroCursorAdapter carroAdapter;


    CarroDatabaseHelper databaseHelper;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.listar_carros, container, false);

        return  view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        databaseHelper = new CarroDatabaseHelper(getActivity());

        carroAdapter = new CarroCursorAdapter(getActivity(), databaseHelper.getAllData());
        setListAdapter(carroAdapter);
    }

}