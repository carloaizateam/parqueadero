/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pragmaticingenieria.entidades;

import java.io.Serializable;
import java.util.List;
/**
 *
 * @author carloaiza
 */public class Parqueadero implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String descripcion;
    private Boolean estado;
    private List<Carro> carroList;
    private ValorParqueadero idValorHora;

    public Parqueadero() {
    }

    public Parqueadero(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }


    public List<Carro> getCarroList() {
        return carroList;
    }

    public void setCarroList(List<Carro> carroList) {
        this.carroList = carroList;
    }

    public ValorParqueadero getIdValorHora() {
        return idValorHora;
    }

    public void setIdValorHora(ValorParqueadero idValorHora) {
        this.idValorHora = idValorHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parqueadero)) {
            return false;
        }
        Parqueadero other = (Parqueadero) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "parqueadero.entidades.Parqueadero[ id=" + id + " ]";
    }
    
}
