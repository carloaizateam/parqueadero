/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pragmaticingenieria.entidades;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author carloaiza
 */
public class Carro implements Serializable {

    private static final long serialVersionUID = 1L;
    private String placa;
    private String color;
    private Date fechaCreacion;
    private Date horaEntrada;
    private Date horaSalida;
    private Boolean estado;
    private Parqueadero idSitioParqueadero;
    private TipoCarro idTipo;

    public Carro() {
    }

    public Carro(String placa, String color) {
        this.placa = placa;
        this.color = color;
    }

    public Carro(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(Date horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Parqueadero getIdSitioParqueadero() {
        return idSitioParqueadero;
    }

    public void setIdSitioParqueadero(Parqueadero idSitioParqueadero) {
        this.idSitioParqueadero = idSitioParqueadero;
    }

    public TipoCarro getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(TipoCarro idTipo) {
        this.idTipo = idTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placa != null ? placa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carro)) {
            return false;
        }
        Carro other = (Carro) object;
        if ((this.placa == null && other.placa != null) || (this.placa != null && !this.placa.equals(other.placa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "parqueadero.entidades.Carro[ placa=" + placa + " ]";
    }
    
}
